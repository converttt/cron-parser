import unittest

from crontab import CronTab

class TestSolution(unittest.TestCase):

    def test_parts(self):
        """
        Test CronTab.parse_part function on each accepted time argument.
        CronTab.PART_MINUTE
        CronTab.PART_HOUR
        CronTab.PART_DAY_OF_MONTH
        CronTab.PART_MONTH
        CronTab.PART_DAY_OF_WEEK
        """

        # Part Minutes
        self.assertEqual(list(map(str, list(range(60)))), CronTab.parse_part(CronTab.PART_MINUTE, '*'))
        self.assertEqual(list(map(str, list(range(60)))), CronTab.parse_part(CronTab.PART_MINUTE, '*/1'))
        self.assertEqual(['0', '15', '30', '45'], CronTab.parse_part(CronTab.PART_MINUTE, '*/15'))
        self.assertEqual(['0', '25', '50'], CronTab.parse_part(CronTab.PART_MINUTE, '*/25'))
        self.assertEqual(['0', '26', '52'], CronTab.parse_part(CronTab.PART_MINUTE, '*/26'))

        self.assertEqual(['0'], CronTab.parse_part(CronTab.PART_MINUTE, '0'))
        self.assertEqual(['26'], CronTab.parse_part(CronTab.PART_MINUTE, '26'))

        self.assertRaises(ValueError, CronTab.parse_part, CronTab.PART_MINUTE, '*/31')
        self.assertRaises(ValueError, CronTab.parse_part, CronTab.PART_MINUTE, '*/60')

        self.assertRaises(ValueError, CronTab.parse_part, CronTab.PART_MINUTE, '60')
        self.assertRaises(ValueError, CronTab.parse_part, CronTab.PART_MINUTE, '600')

        # Part Hour
        self.assertEqual(list(map(str, list(range(24)))), CronTab.parse_part(CronTab.PART_HOUR, '*'))
        self.assertEqual(list(map(str, list(range(24)))), CronTab.parse_part(CronTab.PART_HOUR, '*/1'))
        self.assertEqual(['0', '3', '6', '9', '12', '15', '18', '21'], CronTab.parse_part(CronTab.PART_HOUR, '*/3'))
        self.assertEqual(['0', '6', '12', '18'], CronTab.parse_part(CronTab.PART_HOUR, '*/6'))

        self.assertRaises(ValueError, CronTab.parse_part, CronTab.PART_HOUR, '*/13')
        self.assertRaises(ValueError, CronTab.parse_part, CronTab.PART_HOUR, '*/24')

        # Part Day of Month
        self.assertEqual(list(map(str, list(range(1, 32)))), CronTab.parse_part(CronTab.PART_DAY_OF_MONTH, '*'))
        self.assertEqual(list(map(str, list(range(1, 32)))), CronTab.parse_part(CronTab.PART_DAY_OF_MONTH, '*/1'))
        self.assertEqual(['1', '8', '15', '22', '29'], CronTab.parse_part(CronTab.PART_DAY_OF_MONTH, '*/7'))
        self.assertEqual(['1', '15', '29'], CronTab.parse_part(CronTab.PART_DAY_OF_MONTH, '*/14'))

        self.assertRaises(ValueError, CronTab.parse_part, CronTab.PART_DAY_OF_MONTH, '*/0')
        self.assertRaises(ValueError, CronTab.parse_part, CronTab.PART_DAY_OF_MONTH, '*/32')

        # Part Month
        self.assertEqual(list(map(str, list(range(1, 13)))), CronTab.parse_part(CronTab.PART_MONTH, '*'))
        self.assertEqual(list(map(str, list(range(1, 13)))), CronTab.parse_part(CronTab.PART_MONTH, '*/1'))
        self.assertEqual(['1', '3', '5', '7', '9', '11'], CronTab.parse_part(CronTab.PART_MONTH, '*/2'))
        self.assertEqual(['1', '4', '7', '10'], CronTab.parse_part(CronTab.PART_MONTH, '*/3'))

        self.assertEqual(['1', '6', '9'], CronTab.parse_part(CronTab.PART_MONTH, '1,6,9'))
        self.assertEqual(['4', '5', '6'], CronTab.parse_part(CronTab.PART_MONTH, '4-6'))
        self.assertEqual(['1', '2', '3', '4', '9', '10', '11', '12'], CronTab.parse_part(CronTab.PART_MONTH, '1-4,9-12'))
        self.assertEqual(['1', '2', '5', '6', '7', '10', '11', '12'], CronTab.parse_part(CronTab.PART_MONTH, '1-2,5-7,10-12'))

        self.assertRaises(ValueError, CronTab.parse_part, CronTab.PART_MONTH, '*/0')
        self.assertRaises(ValueError, CronTab.parse_part, CronTab.PART_MONTH, '*/13')
        self.assertRaises(ValueError, CronTab.parse_part, CronTab.PART_MONTH, '*/13')
        self.assertRaises(ValueError, CronTab.parse_part, CronTab.PART_MONTH, '1,6,')
        self.assertRaises(ValueError, CronTab.parse_part, CronTab.PART_MONTH, '0,2')
        self.assertRaises(ValueError, CronTab.parse_part, CronTab.PART_MONTH, '0-2')
        self.assertRaises(ValueError, CronTab.parse_part, CronTab.PART_MONTH, '1-2-3')
        self.assertRaises(ValueError, CronTab.parse_part, CronTab.PART_MONTH, '1-2,5-7,10-13')

        # Day of Week
        self.assertEqual(list(map(str, list(range(7)))), CronTab.parse_part(CronTab.PART_DAY_OF_WEEK, '*'))
        self.assertEqual(list(map(str, list(range(7)))), CronTab.parse_part(CronTab.PART_DAY_OF_WEEK, '*/1'))
        self.assertEqual(['0', '2', '4', '6'], CronTab.parse_part(CronTab.PART_DAY_OF_WEEK, '*/2'))
        self.assertEqual(['0', '3', '6'], CronTab.parse_part(CronTab.PART_DAY_OF_WEEK, '*/3'))

        self.assertRaises(ValueError, CronTab.parse_part, CronTab.PART_DAY_OF_WEEK, '*/7')
        self.assertRaises(ValueError, CronTab.parse_part, CronTab.PART_DAY_OF_WEEK, '*/14')

        # Formatting
        self.assertRaises(ValueError, CronTab.parse_part, 0, '*/')
        self.assertRaises(ValueError, CronTab.parse_part, 0, '*//')
        self.assertRaises(ValueError, CronTab.parse_part, 0, '*/a')
        self.assertRaises(ValueError, CronTab.parse_part, 0, '*1')

    def test_output(self):
        """
        Test CronTab.parse function on parsing a whole command line argument 
        and returing correct results.
        """

        output = """minute        0 15 30 45
hour          0
day of month  1 15
month         1 2 3 4 5 6 7 8 9 10 11 12
day of week   1 2 3 4 5
command       /usr/bin/find"""

        self.assertEqual(output, CronTab.parse('*/15 0 1,15 * 1-5 /usr/bin/find'))

        output = """minute        0 20 40
hour          12
day of month  1 15 16 17 18 19 20
month         1 2 10 11 12
day of week   1 2 3 4 5
command       /usr/bin/exec"""

        self.assertEqual(output, CronTab.parse('*/20 12 1,15-20 1-2,10-12 1-5 /usr/bin/exec'))

        self.assertRaises(ValueError, CronTab.parse, '*/20 12 1,15-20 1-2,10-12 1-5 * /usr/bin/exec')
        self.assertRaises(ValueError, CronTab.parse, '12 1,15-20 1-2,10-12 1-5 /usr/bin/exec')


if __name__ == '__main__':
    unittest.main()