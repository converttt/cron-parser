'''
crontab.py
'''

# Ranges of accepted cron fields
_ranges = [
    (0, 59),
    (0, 23),
    (1, 31),
    (1, 12),
    (0, 6)
]

# Fields of accepted cron fields
_fields = [
    'minute',
    'hour',
    'day of month',
    'month',
    'day of week',
    'command'
]

def _assert(condition, message, *args):
    if not condition:
        raise ValueError(message%args)

class CronTab:
    """
    Abstract class for parsing methods
    """

    PART_MINUTE = 0
    PART_HOUR = 1
    PART_DAY_OF_MONTH = 2
    PART_MONTH = 3
    PART_DAY_OF_WEEK = 4
    PART_COMMAND = 5

    @staticmethod
    def _range(rng, step=1) -> list:
        """
        It creates and returns a list of strings.
        Strings reperesent numbers within a specified range.
        """
        return list(map(str, list(range(rng[0], rng[1] + 1, step))))

    @staticmethod
    def parse(string) -> str:
        """
        It is a main parse method. It receives the whole argument,
        parses it and provides the required results.
        """

        parts = string.split(' ')
        _assert(len(parts) == 6, "invalid string format: %s", string)

        result = []
        for partid in range(5):
            tmp = ' '.join(CronTab.parse_part(partid, parts[partid]))
            tmp = '{:{width}}{:}'.format(_fields[partid], tmp, width=14)

            result.append(tmp)

        tmp = '{:{width}}{:}'.format(_fields[CronTab.PART_COMMAND], parts[CronTab.PART_COMMAND], width=14)
        result.append(tmp)

        return "\n".join(result)

    @staticmethod
    def parse_part(partid, part) -> list:
        """
        It parses a single time field of a cron entry.
        It returns the list of strings, which represent numbers whithin a specified range 
        defined by the given cron field.
        """

        # If the field starts with *: *, */[number]
        if part[0] == '*':
            
            if len(part) == 1:
                return CronTab._range(_ranges[partid])
            
            comp = part.split('/')
            _assert(len(comp) == 2, "improper item specification %s: %s", partid, part)

            step = int(comp[1])
            _assert(step >= _ranges[partid][0] and step <= _ranges[partid][1] // 2 + 1, "invalid step for an item %s: %s", partid, part)
            
            return CronTab._range(_ranges[partid], step)

        # If the number of characters in the field less than 3 
        # assuming that it is a single number: 1, 2, 15 and etc.
        if len(part) < 3:
            
            comp = int(part)
            _assert(comp >= _ranges[partid][0] and comp <= _ranges[partid][1], "improper item specification %s: %s", partid, part)

            return [str(comp)]

        # If the number of characters in the field more than 2
        # assuming that it contains characters `-` or/and `,`: 1-2, 1,4-6 and etc.
        result = []
        for p in part.split(','):

            comp = list(map(int, p.split('-')))

            _assert(len(comp) <= 2, "improper item specification %s: %s", partid, part)
            _assert(comp[0] >= _ranges[partid][0] and comp[0] <= _ranges[partid][1], "improper item specification %s: %s", partid, part)

            if len(comp) == 1:

                result.append(str(comp[0]))
            
            else:

                _assert(comp[1] >= _ranges[partid][0] and comp[1] <= _ranges[partid][1], "improper item specification %s: %s", partid, part)

                result += CronTab._range(comp)

        return result