import sys

from crontab import CronTab

if __name__ == '__main__':
    print(CronTab.parse(sys.argv.pop()))