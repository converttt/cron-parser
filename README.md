# CronTab Parser

## Description
This package intends to offer a method of parsing crontab schedule entries and to output them in a table.

Example input:
```
*/20 12 1,15-20 1-2,10-12 1-5 /usr/bin/exec
```

Example output:
```
minute        0 20 40
hour          12
day of month  1 15 16 17 18 19 20
month         1 2 10 11 12
day of week   1 2 3 4 5
command       /usr/bin/exec
```

## Sample individual crontab fields
It supports only the standard cron format with five time fields (minute, hour, day of month, month, and day of week) plus a command.

Examples of supported entries are as follows:
```
*
*/5
3-25
3,7,9
0-10,30-40
```

## Example uses
Import as a package:
```
>>> from crontab import CronTab
>>> CronTab.parse('*/20 12 1,15-20 1-2,10-12 1-5 /usr/bin/exec')
```

Run as a script:
```
python -m bin.parse '*/15 0 1,15 * 1-5 /usr/bin/find'
```

## Tests
```
python -m test.test_crontab
```