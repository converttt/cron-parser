from distutils.core import setup

setup(name='crontab',
      version='1.0',
      packages=['crontab'],
      classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
)